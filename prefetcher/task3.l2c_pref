/*
  L2C GHB-based Stride Prefetcher
  @author sbuehrer
 */

#include "cache.h"

#define X 8
#define IT_SIZE 256
#define GHB_SIZE 256
#define LOOK_AHEAD 4
#define PREFETCH_DEGREE 4

class GHB{
    public:
        // the global miss address
        uint64_t global_addr;

        // the link pointer
        GHB *prev_ptr;

        GHB(){
            global_addr=0;
            prev_ptr=NULL;
        }
};

class IT {
    public:
        // the load instruction’s PC
        uint64_t ip;
        
        // use LRU to evict old PC
    	uint32_t lru;

        // the pointer into the Global History Buffer
        GHB *ghb_ptr;

        IT(){
            ip=0;
            lru=0;
            ghb_ptr=NULL;
        }
};


IT index_table[IT_SIZE];
GHB ghb_table[GHB_SIZE];
GHB *head_ptr = &ghb_table[0];


void CACHE::l2c_prefetcher_initialize() 
{
    cout << "CPU " << cpu << " L2C GHB-based Stride Prefetcher" << endl;
    for (int i=0; i<IT_SIZE; i++){
        index_table[i].lru = i;
    }
}

uint32_t CACHE::l2c_prefetcher_operate(uint64_t addr, uint64_t ip, uint8_t cache_hit, uint8_t type, uint32_t metadata_in)
{
    // seach for the IP in the IT
    uint64_t cl_addr = addr >> LOG2_BLOCK_SIZE;
    int index = -1;
    for (index=0; index<IT_SIZE; index++) {
        if (index_table[index].ip == ip)
            break;
    }
    
    // this is a new IP that doesn't have a index table entry yet, so allocate one
    if (index == IT_SIZE) {
        for (index=0; index<IT_SIZE; index++) {
            if (index_table[index].lru == (IT_SIZE-1))
                break;
        }
        
        // the miss address is placed into the GHB table pointed to by the head pointer , and 
        // its  link  entry is given the current value in the index table.
        head_ptr->global_addr = cl_addr;
        head_ptr->prev_ptr = NULL;

        // the IT entry is updated with the head pointer, which points to the newly added entry.
        index_table[index].ghb_ptr = head_ptr;
        index_table[index].ip = ip;

        // the lru also gets updated
        for (int i=0; i<IT_SIZE; i++) {
            if (index_table[i].lru < index_table[index].lru)
                index_table[i].lru++;
        }
        index_table[index].lru = 0;

        // finally, the head pointer is incremented to point to the next GHB entry.
        head_ptr = (head_ptr<=&ghb_table[GHB_SIZE-1]) ? head_ptr+1:  &ghb_table[0]; // just an extremly fancy way to make a circulant FIFO queue

        // cout << "[IT Entry] created: " << index <<" ip: " << hex << ip << " ptr: "<< hex <<index_table[index].ghb_ptr<<endl;
        return metadata_in;
    }

    // sanity check
    // at this point we should know a matching IT entry

    if (index == -1)
        assert(0);

    // Add an entry into GHB and update head_ptr
    // cout << "[IT Entry] hit: " << index <<" ip: " << hex << ip << " ptr: "<< hex <<index_table[index].ghb_ptr<<endl;
	head_ptr->global_addr = cl_addr;
	head_ptr->prev_ptr = index_table[index].ghb_ptr;
	index_table[index].ghb_ptr = head_ptr;
    head_ptr = (head_ptr<=&ghb_table[GHB_SIZE-1]) ? head_ptr+1:  &ghb_table[0]; // just an extremly fancy way to make a circulant FIFO queue


    // compute the strides
    GHB* head = index_table[index].ghb_ptr;
    uint64_t pref_stride=0;
    uint64_t strides[X];


    for(int i=0;i<X;i++){
        GHB* prev_head=head->prev_ptr;
        if(prev_head==NULL) return metadata_in;
        strides[i] = (head->global_addr) - (prev_head->global_addr);
        pref_stride = strides[i];
        head=prev_head;
    }
    
    uint64_t count=0;
    uint64_t topCount=0;

    for(int i=0;i<X;i++){
        count=0;
        for(int j=0;j<X;j++){
            if (strides[i]==strides[j]) count++;
        }
        if (count>topCount){
            topCount=count;
            pref_stride=strides[i];
        }
    }

    for (int i=0; i<PREFETCH_DEGREE; i++) {
        uint64_t pf_address = (cl_addr + (pref_stride*(i+LOOK_AHEAD))) << LOG2_BLOCK_SIZE;

        // only issue a prefetch if the prefetch address is in the same 4 KB page 
        // as the current demand access address
        if ((pf_address >> LOG2_PAGE_SIZE) != (addr >> LOG2_PAGE_SIZE))
            break;

        // cout <<"address: " << hex << addr << " pf_address: " << hex << pf_address << endl;

        // check the MSHR occupancy to decide if we're going to prefetch to the L2 or LLC
        if (MSHR.occupancy < (MSHR.SIZE>>1))
        prefetch_line(ip, addr, pf_address, FILL_L2, 0);
        else
        prefetch_line(ip, addr, pf_address, FILL_LLC, 0);
    }


    // the lru gets updated
    for (int i=0; i<IT_SIZE; i++) {
        if (index_table[i].lru < index_table[index].lru)
            index_table[i].lru++;
    }
    index_table[index].lru = 0;

    return metadata_in;
}

uint32_t CACHE::l2c_prefetcher_cache_fill(uint64_t addr, uint32_t set, uint32_t way, uint8_t prefetch, uint64_t evicted_addr, uint32_t metadata_in)
{
  return metadata_in;
}

void CACHE::l2c_prefetcher_final_stats()
{
    cout << "CPU " << cpu << " L2C GHB-based stride prefetcher final stats" << endl;
}
